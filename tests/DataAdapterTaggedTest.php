<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Eventize\Cache\DataAdapterTagged;

class DataAdapterTaggedTest extends PHPUnit_Framework_TestCase
{
    public function testDataAdapter()
    {
        $item = new DataAdapterTagged('test-key-1', 'test-domain', true, 'any-value');

        $this->assertFalse($item->isTagged());
        $item->setTags(['tag-1', 'tag-2']);
        $this->assertTrue($item->isTagged());
        $this->assertEquals(['tag-1', 'tag-2'], $item->getTags());
        $this->assertEquals(['tag-1' => null, 'tag-2' => null], $item->getTagsVersions());

        $item->setTagsVersions(['tag-1' => 'v1', 'tag-2' => 'v1']);
        $this->assertEquals(['tag-1', 'tag-2'], $item->getTags());
        $this->assertEquals(['tag-1' => 'v1', 'tag-2' => 'v1'], $item->getTagsVersions());
    }
}