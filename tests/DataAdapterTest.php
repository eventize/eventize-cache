<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Eventize\Cache\DataAdapter;

class DataAdapterTest extends PHPUnit_Framework_TestCase
{
    public function testDataAdapter()
    {
        $item = new DataAdapter('test-key-1', 'test-domain', false, 'any-value');
        $this->assertEquals('test-key-1', $item->getKey());
        $this->assertEquals('test-domain', $item->getDomain());
        $this->assertFalse($item->isHit());
        $this->assertNull($item->get());

        $item->set('test-value');
        $this->assertEquals('test-value', $item->get());

        $this->assertEquals('"test-value"', json_encode($item));
        unset($item);

        $item = new DataAdapter('test-key-1', 'test-domain', true, 'any-value');
        $this->assertEquals('test-key-1', $item->getKey());
        $this->assertEquals('test-domain', $item->getDomain());
        $this->assertTrue($item->isHit());
        $this->assertEquals('any-value', $item->get());

        $item->invalidate();
        $this->assertEquals('test-key-1', $item->getKey());
        $this->assertEquals('test-domain', $item->getDomain());
        $this->assertFalse($item->isHit());
        $this->assertNull($item->get());

        $this->assertEquals('null', json_encode($item));
        unset($item);

        $item = new DataAdapter('test-key-1', 'test-domain');
        $this->assertNull($item->getTTL());
        $item->expiresAfter(10);
        $this->assertEquals(10, $item->getTTL());

        $item2 = new DataAdapter('test-key-2', 'test-domain');
        $item2->expiresAt(new \DateTimeImmutable('+10 second'));
        $this->assertEquals(10, $item2->getTTL());

        $item3 = new DataAdapter('test-key-3', 'test-domain');
        $item3->expiresAt(new \DateTimeImmutable('-1 second'));
        $this->assertEquals(-1, $item3->getTTL());

        $item4 = new DataAdapter('test-key-4', 'test-domain');
        $item4->expiresAfter(new \DateInterval('PT10S'));
        $this->assertEquals(10, $item4->getTTL());
        sleep(1);
        $this->assertEquals(9, $item->getTTL());
        $this->assertEquals(9, $item2->getTTL());
        $this->assertEquals(-2, $item3->getTTL());
        $this->assertEquals(9, $item4->getTTL());
    }
}