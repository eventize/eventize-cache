<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


use Eventize\Cache\CacheTagManager;
use Eventize\Cache\DataAdapter\DataAdapterInterface;
use Eventize\Cache\Exception\CacheManagerUndefinedTagProviderException;
use Eventize\Cache\Provider\CacheProviderInterface;

class CacheManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockBuilder
     */
    private $itemCacheProviderMock;

    /**
     * @var CacheProviderInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $itemCacheProvider;

    /**
     * @var PHPUnit_Framework_MockObject_MockBuilder
     */
    private $tagCacheProviderMock;

    /**
     * @var CacheProviderInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private $tagCacheProvider;

    /**
     * @var PHPUnit_Framework_MockObject_MockBuilder
     */
    private $cacheManagerMock;

    /**
     * @var CacheTagManager|PHPUnit_Framework_MockObject_MockObject
     */
    private $cacheManager;

    /**
     *
     */
    protected function setUp()
    {
        $this->itemCacheProviderMock = $this->getMockBuilder(CacheProviderInterface::class)
            ->setMockClassName('ItemCacheProvider_Mock');

        $this->tagCacheProviderMock = $this->getMockBuilder(CacheProviderInterface::class)
            ->setMockClassName('TagCacheProvider_Mock');

        $this->cacheManagerMock = $this->getMockBuilder(CacheTagManager::class)->setMethods(null);
    }

    protected function tearDown()
    {
        $this->itemCacheProviderMock = null;
        $this->tagCacheProviderMock = null;
        $this->cacheManager = null;
    }

    private function createCacheManager()
    {
        $this->itemCacheProvider = $this->itemCacheProviderMock->getMockForAbstractClass();
        $this->tagCacheProvider = $this->tagCacheProviderMock->getMockForAbstractClass();

        $this->cacheManager = $this->cacheManagerMock->setConstructorArgs(
            [
                $this->itemCacheProvider,
                $this->tagCacheProvider,
            ]
        )->getMock();
    }

    public function testGetProviders()
    {
        $this->createCacheManager();

        $this->assertEquals($this->itemCacheProvider, $this->cacheManager->getItemProvider());
        $this->assertNotEquals($this->tagCacheProvider, $this->cacheManager->getItemProvider());

        $this->assertTrue($this->cacheManager->hasTagProvider());

        $tagProvider = $this->cacheManager->getTagProvider();
        $this->assertEquals($this->tagCacheProvider, $tagProvider);
        $this->assertNotEquals($this->itemCacheProvider, $tagProvider);

        $this->cacheManagerMock->setMethods(['hasTagProvider']);
        $this->createCacheManager();

        $this->cacheManager->expects($this->once())->method('hasTagProvider')->willReturn(false);
        $this->expectException(CacheManagerUndefinedTagProviderException::class);
        $this->cacheManager->getTagProvider();
    }

    public function testCreateSingleAndMultiplyItem()
    {
        $this->createCacheManager();

        $this->assertInstanceOf(DataAdapterInterface::class, $this->cacheManager->createItem('test-key-1'));

        $itemCollection = $this->cacheManager->createItems('test-key-1', 'test-key-2');
        $this->assertInstanceOf(DataAdapterInterface::class, $itemCollection['test-key-1']);
        $this->assertInstanceOf(DataAdapterInterface::class, $itemCollection['test-key-2']);
    }

    public function testGetSingleItem()
    {
        $this->itemCacheProviderMock->setMethods(['getItem']);
        $this->tagCacheProviderMock->setMethods(['getItems']);
        $this->createCacheManager();

        $dataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $tagDataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $this->itemCacheProvider->expects($this->once())->method('getItem')->willReturn($dataAdapter);
        $dataAdapter->expects($this->once())->method('isHit')->willReturn(true);
        $dataAdapter->expects($this->once())->method('hasTags')->willReturn(true);
        $dataAdapter->expects($this->once())->method('getTagsVersions')->willReturn(
            [
                'test-tag-1' => '1',
            ]
        );

        $this->tagCacheProvider->expects($this->once())->method('getItems')->willReturn(
            [
                'test-tag-1' => $tagDataAdapter
            ]
        );
        $tagDataAdapter->expects($this->any())->method('getKey')->willReturn('test-tag-1');
        $tagDataAdapter->expects($this->any())->method('getData')->willReturn('1');

        $this->assertEquals($dataAdapter, $this->cacheManager->getItem('test-key'));
    }

    public function testGetSingleItemInvalidTag()
    {
        $this->itemCacheProviderMock->setMethods(['getItem']);
        $this->tagCacheProviderMock->setMethods(['getItems']);
        $this->createCacheManager();

        $dataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $tagDataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $this->itemCacheProvider->expects($this->once())->method('getItem')->willReturn($dataAdapter);
        $dataAdapter->expects($this->once())->method('isHit')->willReturn(true);
        $dataAdapter->expects($this->once())->method('hasTags')->willReturn(true);
        $dataAdapter->expects($this->once())->method('getTagsVersions')->willReturn([
            'test-tag-1' => '1',
        ]);
        $dataAdapter->expects($this->once())->method('invalidate');

        $this->tagCacheProvider->expects($this->once())->method('getItems')->willReturn([
            'test-tag-1' => $tagDataAdapter
        ]);
        $tagDataAdapter->expects($this->any())->method('getKey')->willReturn('test-tag-1');
        $tagDataAdapter->expects($this->any())->method('getData')->willReturn('2');
        $this->assertEquals($dataAdapter, $this->cacheManager->getItem('test-key'));
    }

    public function testSetSingleItem()
    {
        $this->tagCacheProviderMock->setMethods(['getItems', 'saveDeferred', 'commit']);
        $this->createCacheManager();

        /** @var DataAdapterInterface|PHPUnit_Framework_MockObject_MockObject $dataAdapter */
        $dataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        /** @var DataAdapterInterface|PHPUnit_Framework_MockObject_MockObject $dataAdapter */
        $tagDataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $dataAdapter->expects($this->any())->method('hasTags')->willReturn(true);
        $dataAdapter->expects($this->once())->method('getTags')->willReturn(['test-tag-1']);

        $this->tagCacheProvider->expects($this->once())->method('getItems')->willReturn([
            'test-tag-1' => $tagDataAdapter
        ]);

        $tagDataAdapter->expects($this->any())->method('isHit')->willReturn(false);
        $tagDataAdapter->expects($this->any())->method('getKey')->willReturn('test-tag-1');

        $tagDataAdapter->expects($this->once())->method('setData')
            ->willReturnCallback(function($tagVersion) use ($dataAdapter, $tagDataAdapter) {
                $tagDataAdapter->expects($this->any())->method('getData')->willReturn($tagVersion);

                $dataAdapter->expects($this->once())->method('setTagsVersions')
                    ->with(['test-tag-1' => $tagVersion])->willReturnSelf();

                return $tagDataAdapter;
            });

        $this->tagCacheProvider->expects($this->once())->method('saveDeferred')->willReturnSelf();
        $this->tagCacheProvider->expects($this->once())->method('commit')->willReturn(true);

        $this->cacheManager->setItem($dataAdapter);

    }

    public function testSetSingleItemDeferred()
    {
        $this->tagCacheProviderMock->setMethods(['getItems', 'saveDeferred', 'commit']);
        $this->createCacheManager();

        /** @var DataAdapterInterface|PHPUnit_Framework_MockObject_MockObject $dataAdapter */
        $dataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $this->cacheManager->setItem($dataAdapter, true);

        /** @var DataAdapterInterface|PHPUnit_Framework_MockObject_MockObject $dataAdapter */
        $tagDataAdapter = $this->getMockBuilder(DataAdapterInterface::class)
            ->getMockForAbstractClass();

        $dataAdapter->expects($this->any())->method('hasTags')->willReturn(true);
        $dataAdapter->expects($this->once())->method('getTags')->willReturn(['test-tag-1']);
        $dataAdapter->expects($this->once())->method('getTagsVersions')->willReturn(['test-tag-1' => null]);

        $this->tagCacheProvider->expects($this->once())->method('getItems')->willReturn([
            'test-tag-1' => $tagDataAdapter
        ]);

        $tagDataAdapter->expects($this->any())->method('isHit')->willReturn(false);
        $tagDataAdapter->expects($this->any())->method('getKey')->willReturn('test-tag-1');

        $tagDataAdapter->expects($this->once())->method('setData')
            ->willReturnCallback(function($tagVersion) use ($dataAdapter, $tagDataAdapter) {
                $tagDataAdapter->expects($this->any())->method('getData')->willReturn($tagVersion);

                $dataAdapter->expects($this->once())->method('setTagsVersions')
                    ->with(['test-tag-1' => $tagVersion])->willReturnSelf();

                return $tagDataAdapter;
            });

        $this->tagCacheProvider->expects($this->once())->method('saveDeferred')->willReturnSelf();
        $this->tagCacheProvider->expects($this->once())->method('commit')->willReturn(true);

        $this->cacheManager->commit();
    }

    public function testDeleteSingleItem()
    {
        $this->itemCacheProviderMock->setMethods(['deleteItem']);
        $this->createCacheManager();

        $this->itemCacheProvider->expects($this->once())->method('deleteItem')->with('test-key')->willReturn(true);
        $this->assertTrue($this->cacheManager->deleteItem('test-key'));
    }

    public function testDeleteSingleItemDeferred()
    {
        $this->itemCacheProviderMock->setMethods(['deleteItem', 'deleteItems']);
        $this->createCacheManager();

        $this->itemCacheProvider->expects($this->exactly(0))->method('deleteItem');
        $this->assertTrue($this->cacheManager->deleteItem('test-key', true));

        $this->itemCacheProvider->expects($this->once())->method('deleteItems')
            ->with(['test-key'])->willReturn(true);

        $this->assertTrue($this->cacheManager->commit());
    }

    public function testGetMultiplyItems()
    {
        
    }

    public function testGetMultiplyItemsInvalidTags()
    {

    }

    public function testSetMultiplyItem()
    {
        
    }

    public function testSetMultiplyItemDeferred()
    {

    }

    public function testDeleteMultiplyItems()
    {
        $this->itemCacheProviderMock->setMethods(['deleteItems']);
        $this->createCacheManager();

        $this->itemCacheProvider->expects($this->once())->method('deleteItems')
            ->with(['test-key-1', 'test-key-2'])->willReturn(true);
        $this->assertTrue($this->cacheManager->deleteItems(['test-key-1', 'test-key-2']));
    }

    public function testDeleteMultiplyItemsDeferred()
    {
        $this->itemCacheProviderMock->setMethods(['deleteItems']);
        $this->createCacheManager();

        $this->assertTrue($this->cacheManager->deleteItems(['test-key-1', 'test-key-2'], true));

        $this->itemCacheProvider->expects($this->once())->method('deleteItems')
            ->with(['test-key-1', 'test-key-2'])->willReturn(true);

        $this->assertTrue($this->cacheManager->commit());
    }

    public function testDeleteTags()
    {
        $this->tagCacheProviderMock->setMethods(['deleteItems']);
        $this->createCacheManager();

        $this->tagCacheProvider->expects($this->once())->method('deleteItems')
            ->with(['test-tag-1', 'test-tag-2'])->willReturn(true);
        $this->assertTrue($this->cacheManager->deleteTags(['test-tag-1', 'test-tag-2']));
    }

    public function testDeleteTagsDeferred()
    {
        $this->tagCacheProviderMock->setMethods(['deleteItems']);
        $this->createCacheManager();

        $this->assertTrue($this->cacheManager->deleteTags(['test-tag-1', 'test-tag-2'], true));

        $this->tagCacheProvider->expects($this->once())->method('deleteItems')
            ->with(['test-tag-1', 'test-tag-2'])->willReturn(true);

        $this->assertTrue($this->cacheManager->commit());
    }

    public function testExpireTags()
    {
        
    }

    public function testExpireTagsDeferred()
    {

    }
}