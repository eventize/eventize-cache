<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


use Psr\Cache\CacheItemInterface;

interface DataAdapterInterface extends CacheItemInterface, \JsonSerializable
{
    /**
     * DataAdapterInterface constructor.
     * 
     * @param string $key
     * @param string $domain
     * @param bool $isHit
     * @param mixed $value
     */
    public function __construct($key, $domain, $isHit = false, $value = null);

    /**
     * @return string
     */
    public function getDomain() : string;

    /**
     * @return static
     */
    public function invalidate();

    /**
     * @return int|null
     */
    public function getTTL();
}