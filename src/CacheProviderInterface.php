<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


use Psr\Cache\CacheItemPoolInterface;

/**
 * Interface CacheProviderInterface
 * @package Eventize\Cache
 *
 * @author Sevastianov Andrey <me@andrej.in.ua>
 */
interface CacheProviderInterface extends CacheItemPoolInterface
{
    public function getDomain();

    /**
     * @param string $key string
     * @return DataAdapterInterface
     */
    public function createItem($key) : DataAdapterInterface;

    /**
     * Create DataAdapterInterface collection for $keys
     *
     * @param array $keys
     * @return DataAdapterInterface[]
     */
    public function createItems($keys) : array;

    /**
     * @param string $key
     * @return DataAdapterInterface
     */
    public function getItem($key) : DataAdapterInterface;

    /**
     * @param array $keys
     * @return DataAdapterInterface[]
     */
    public function getItems(array $keys = []) : array;
}