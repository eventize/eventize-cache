<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


class DataAdapter implements DataAdapterInterface
{
    /**
     * @var bool
     */
    private $isHit;

    /**
     * @var string
     */
    private $key;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var \DateTimeImmutable
     */
    private $expiresAt;

    /**
     * @var string
     */
    private $domain;

    /**
     * DataAdapter constructor.
     *
     * @param string $key
     * @param string $domain
     * @param bool $isHit
     * @param string $value
     */
    public function __construct($key, $domain, $isHit = false, $value = null)
    {
        $this->key = $key;
        $this->domain = $domain;
        $this->isHit = $isHit;

        if ($isHit) {
            $rawValue = json_decode($value, true);
            $this->set($rawValue !== null || $value === 'null' ? $rawValue : $value);
        }
    }

    /**
     * @return string
     */
    public function getKey() : string
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isHit() : bool
    {
        return $this->isHit;
    }

    /**
     * @inheritdoc
     */
    public function set($value) : self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function expiresAt($expiration = null) : self
    {
        if ($expiration === null) {
            $this->expiresAt = null;
        } elseif ($expiration instanceof \DateTimeImmutable) {
            $this->expiresAt = $expiration;
        } elseif ($expiration instanceof \DateTime) {
            $this->expiresAt = \DateTimeImmutable::createFromMutable($expiration);
        } else {
            throw new \InvalidArgumentException('Incorrect expires at argument');
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function expiresAfter($time = null) : DataAdapterInterface
    {
        $expiration = new \DateTimeImmutable();

        if ($time instanceof \DateInterval) {
            return $this->expiresAt($expiration->add($time));
        }

        if (is_scalar($time)) {
            return $this->expiresAt($expiration->setTimestamp($expiration->getTimestamp() + $time));
        }

        throw new \InvalidArgumentException('Incorrect expires after argument');
    }

    /**
     * @return string
     */
    public function getDomain() : string
    {
        return $this->domain;
    }

    /**
     * @return static
     */
    public function invalidate()
    {
        $this->isHit = false;
        $this->value = null;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTTL()
    {
        return $this->expiresAt ? $this->expiresAt->getTimestamp() - time() : null;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return $this->get();
    }
}