<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


use Eventize\Cache\Exception\CacheManagerUndefinedTagProviderException;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Interface CacheManagerInterface
 * @package Eventize\Cache
 *
 * @author Sevastianov Andrey <me@andrej.in.ua>
 */
interface CacheTagManagerInterface extends CacheItemPoolInterface
{
    public function __construct(
        CacheProviderInterface $itemProvider,
        CacheProviderInterface $tagProvider
    );

    /**
     * Low level api, return item provider
     * 
     * @return CacheProviderInterface
     */
    public function getItemProvider() : CacheProviderInterface;

    /**
     * Low level api, return tag provider
     * 
     * @return CacheProviderInterface
     */
    public function getTagProvider() : CacheProviderInterface;

    /**
     * @param string $key string
     * @return DataAdapterTaggedInterface
     */
    public function createItem($key) : DataAdapterTaggedInterface;

    /**
     * Create DataAdapterInterface collection for $keys
     *
     * @param array $keys
     * @return DataAdapterTaggedInterface[]
     */
    public function createItems($keys) : array;

    /**
     * @param string $key
     * @return DataAdapterTaggedInterface
     */
    public function getItem($key) : DataAdapterTaggedInterface;

    /**
     * @param array $keys
     * @return DataAdapterTaggedInterface[]
     */
    public function getItems(array $keys = []) : array;

    /**
     * Remove tags items from storage
     *
     * @param array $tags
     * @return bool
     */
    public function deleteTags(array $tags) : bool;

    /**
     * @param array $tags
     * @return array
     *     New tag versions
     */
    public function expireTags(array $tags) : array;
}