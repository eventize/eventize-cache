<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache\Helper;


use Eventize\Cache\DataAdapterTagged;

trait CacheTaggedGeneratorTrait
{
    use CacheHelperTrait;

    /**
     * Get from cache or generate for single key
     * 
     * @param string $key
     * @param callable $generator
     * @param int|null $ttl
     * @param array $tags
     * @return mixed
     */
    protected function getCachedTaggedValue($key, callable $generator, $ttl = null, array $tags = [])
    {
        if ($this->isSetCacheTagManager()) {
            $provider = $this->getCacheTagManager();
            $item = $provider->getItem($key);
            if (!$item->isHit()) {
                $item->set(call_user_func($generator))
                    ->setTags($tags)
                    ->expiresAfter($ttl);

                $provider->save($item);
            }

            return $item->get();
        } else {
            return call_user_func($generator);
        }
    }

    /**
     * Get from tagged cache or generate for multiply keys
     *
     * @param callable[] $map
     *      use cache key as map keys and list of [callable $generator, $ttl, array $tags] as map values
     * @return array
     */
    protected function getCachedTaggedValues(array $map) : array
    {
        $response = [];
        if ($this->isSetCacheTagManager()) {
            $unsaved = false;
            $provider = $this->getCacheTagManager();
            /** @var DataAdapterTagged $item */
            foreach ($provider->getItems(array_keys($map)) as $item) {
                assert($item instanceof DataAdapterTagged, 'CacheTagManagerInterface::getItems must response array of DataAdapterTagged');
                assert(is_callable($map[$item->getKey()][0]), 'Generator must placed in 0 index of map');
                if (!$item->isHit()) {
                    $item
                        ->set(call_user_func($map[$item->getKey()][0]))
                        ->expiresAfter($map[$item->getKey()][1])
                        ->setTags($map[$item->getKey()][2]);

                    $provider->saveDeferred($item);
                    $unsaved = true;
                }

                $response[$item->getKey()] = $item->get();
            }

            if ($unsaved) {
                $provider->commit();
            }
        } else {
            foreach ($map as $key => $list) {
                assert(is_callable($list[0]), 'Generator must placed in 0 index of map');
                $response[$key] = call_user_func($list[0]);
            }
        }

        return $response;
    }
}