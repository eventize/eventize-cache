<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache\Helper;


use Eventize\Cache\CacheProviderInterface;
use Eventize\Cache\CacheTagManager;
use Psr\Cache\CacheItemPoolInterface;

trait CacheHelperTrait
{
    /**
     * @var CacheItemPoolInterface
     */
    private $cacheItemPool;

    /**
     * @param CacheItemPoolInterface $itemPool
     * @return static
     */
    public function setCacheItemPool(CacheItemPoolInterface $itemPool)
    {
        $this->cacheItemPool = $itemPool;
        return $this;
    }

    /**
     * @return bool
     */
    protected function isSetCacheItemPool()
    {
        return (bool)$this->cacheItemPool;
    }

    /**
     * @return CacheItemPoolInterface
     */
    protected function getCacheItemPool() : CacheItemPoolInterface
    {
        assert((bool)$this->cacheItemPool, 'Cache item pool not set');
        return $this->cacheItemPool;
    }

    /**
     * @return bool
     */
    protected function isSetCacheProvider()
    {
        return $this->cacheItemPool && $this->cacheItemPool instanceof CacheProviderInterface;
    }

    /**
     * @return CacheProviderInterface
     */
    protected function getCacheProvider() : CacheProviderInterface
    {
        assert($this->cacheItemPool && $this->cacheItemPool instanceof CacheProviderInterface, 'Cache provider not set');
        return $this->cacheItemPool;
    }

    /**
     * @return bool
     */
    protected function isSetCacheTagManager()
    {
        return $this->cacheItemPool && $this->cacheItemPool instanceof CacheTagManager;
    }

    /**
     * @return CacheTagManager
     */
    protected function getCacheTagManager() : CacheTagManager
    {
        assert($this->cacheItemPool && $this->cacheItemPool instanceof CacheTagManager, 'Cache tag manager not set');
        return $this->cacheItemPool;
    }
}