<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache\Helper;


use Psr\Cache\CacheItemInterface;

trait CacheGeneratorTrait
{
    use CacheHelperTrait;

    /**
     * Get from cache or generate for single key
     *
     * @param string $key
     * @param callable $generator
     * @param int|null $ttl
     * @return mixed
     */
    protected function getCachedValue($key, callable $generator, $ttl = null)
    {
        if ($this->isSetCacheItemPool()) {
            $provider = $this->getCacheItemPool();
            $item = $provider->getItem($key);
            if (!$item->isHit()) {
                $item->set(call_user_func($generator))
                    ->expiresAfter($ttl);

                $provider->save($item);
            }

            return $item->get();
        } else {
            return call_user_func($generator);
        }
    }

    /**
     * Get from cache or generate for multiply keys
     * 
     * @param callable[] $map
     *      use cache key as map keys and list of [callable $generator, $ttl] as map values
     * @return array
     */
    protected function getCachedValues(array $map) : array
    {
        $response = [];
        if ($this->isSetCacheItemPool()) {
            $unsaved = false;
            $provider = $this->getCacheItemPool();
            /** @var CacheItemInterface $item */
            foreach ($provider->getItems(array_keys($map)) as $item) {
                if (!$item->isHit()) {
                    assert(is_callable($map[$item->getKey()][0]), 'Generator must placed in 0 index of map');
                    $item
                        ->set(call_user_func($map[$item->getKey()][0]))
                        ->expiresAfter($map[$item->getKey()][1]);
                    
                    $provider->saveDeferred($item);
                    $unsaved = true;
                }

                $response[$item->getKey()] = $item->get();
            }

            if ($unsaved) {
                $provider->commit();
            }
        } else {
            foreach ($map as $key => $list) {
                assert(is_callable($list[0]), 'Generator must placed in 0 index of map');
                $response[$key] = call_user_func($list[0]);
            }
        }

        return $response;
    }
}