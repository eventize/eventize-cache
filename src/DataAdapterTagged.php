<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


class DataAdapterTagged extends DataAdapter implements DataAdapterTaggedInterface
{
    const STRUCTURE_KEY_VALUE = 0;
    const STRUCTURE_KEY_TAGS = 1;

    /**
     * @var array
     */
    protected $tags = [];

    /**
     * DataAdapterTagged constructor.
     * @param string $key
     * @param string $domain
     * @param bool $isHit
     * @param string $value
     */
    public function __construct($key, $domain, $isHit = false, $value = null)
    {
        parent::__construct($key, $domain, $isHit, $value);

        if ($isHit) {
            $rawValue = $this->get();
            if (
                is_array($rawValue)
                && count($rawValue) === 2
                && array_intersect_key(
                    $rawValue,
                    [
                        static::STRUCTURE_KEY_VALUE => null,
                        static::STRUCTURE_KEY_TAGS => null,
                    ]
                )
            ) {
                $this->set($rawValue[static::STRUCTURE_KEY_VALUE]);
                if (is_array($rawValue[self::STRUCTURE_KEY_TAGS])) {
                    $this->setTagsVersions($rawValue[self::STRUCTURE_KEY_TAGS]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function setTags(array $tags)
    {
        $this->tags = array_fill_keys($tags, null);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTags() : array
    {
        return array_keys($this->tags);
    }

    /**
     * @inheritdoc
     */
    public function isTagged() : bool
    {
        return count($this->tags) > 0;
    }

    public function setTagsVersions(array $versions)
    {
        $this->tags = $versions;

        return $this;
    }

    public function getTagsVersions() : array
    {
        return $this->tags;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return ($this->isTagged()
            ? [
                static::STRUCTURE_KEY_VALUE => $this->get(),
                static::STRUCTURE_KEY_TAGS => $this->getTagsVersions(),
            ]
            : $this->get()
        );
    }
}