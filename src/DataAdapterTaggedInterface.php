<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


interface DataAdapterTaggedInterface extends DataAdapterInterface
{
    /**
     * @param array $tags
     * @return DataAdapterTaggedInterface
     */
    public function setTags(array $tags);

    /**
     * @return array
     */
    public function getTags() : array;

    /**
     * @return bool
     */
    public function isTagged() : bool;

    /**
     * @param array $versions
     * @return DataAdapterTaggedInterface
     */
    public function setTagsVersions(array $versions);

    /**
     * @return array
     */
    public function getTagsVersions() : array;
}