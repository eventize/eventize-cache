<?php

/*
 * This file is part of the Eventize package.
 *
 * (c) Sevastianonv Andrey <me@andrej.in.ua>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eventize\Cache;


use Eventize\Cache\Exception\CacheException;
use Eventize\Cache\Exception\CacheItemPoolExistItemException;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\InvalidArgumentException;

class CacheTagManager implements CacheTagManagerInterface
{
    /**
     * @var string
     */
    private static $sessionTagPrefix;

    /**
     * @var CacheProviderInterface
     */
    private $itemProvider;

    /**
     * @var CacheProviderInterface
     */
    private $tagProvider;

    /**
     * @var CacheItemInterface[]
     */
    private $sessionTagsItems = [];

    /**
     * @var CacheItemInterface[]
     */
    private $deferredItems = [];

    /**
     * @inheritdoc
     */
    public function __construct(
        CacheProviderInterface $itemProvider,
        CacheProviderInterface $tagProvider
    ) {
        $this->itemProvider = $itemProvider;
        $this->tagProvider = $tagProvider;
    }

    /**
     * @inheritdoc
     */
    public function getItemProvider() : CacheProviderInterface
    {
        return $this->itemProvider;
    }

    /**
     * @inheritdoc
     */
    public function getTagProvider() : CacheProviderInterface
    {
        return $this->tagProvider;
    }

    /**
     * @inheritdoc
     */
    public function createItem($key) : DataAdapterTaggedInterface
    {
        return $this->itemProvider->createItem($key);
    }

    /**
     * @inheritdoc
     */
    public function createItems($keys) : array
    {
        return $this->itemProvider->createItems($keys);
    }

    /**
     * Returns a Cache Item representing the specified key.
     *
     * This method must always return a CacheItemInterface object, even in case of
     * a cache miss. It MUST NOT return null.
     *
     * @param string $key
     *   The key for which to return the corresponding Cache Item.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return DataAdapterTaggedInterface
     *   The corresponding Cache Item.
     */
    public function getItem($key) : DataAdapterTaggedInterface
    {
        $item = $this->itemProvider->getItem($key);
        if ($item->isHit() && $item instanceof DataAdapterTaggedInterface && $item->isTagged()) {
            $tags = $item->getTagsVersions();
            $tagsItems = $this->getTagsItems(array_keys($tags));
            foreach ($tagsItems as $tag) {
                if (!$tag->isHit() || $tag->get() != $tags[$tag->getKey()]) {
                    $item->invalidate();
                    break;
                }
            }
        }

        return $item;
    }

    /**
     * Returns a traversable set of cache items.
     *
     * @param string[] $keys
     *   An indexed array of keys of items to retrieve.
     *
     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return CacheItemInterface[]
     *   A traversable collection of Cache Items keyed by the cache keys of
     *   each item. A Cache item will be returned for each key, even if that
     *   key is not found. However, if no keys are specified then an empty
     *   traversable MUST be returned instead.
     */
    public function getItems(array $keys = array()) : array
    {
        $items = $this->itemProvider->getItems($keys);

        /** @var DataAdapterTaggedInterface[] $taggedItems */
        $taggedItems = [];
        $tagsMap = [];
        foreach ($items as $item) {
            if ($item->isHit() && $item instanceof DataAdapterTaggedInterface && $item->isTagged()) {
                $taggedItems[] = $item;
                $tagsMap[] = $item->getTags();
            }
        }

        if ($taggedItems && $tagsMap) {
            $tagsItems = $this->getTagsItems(array_merge(...$tagsMap));
            foreach ($taggedItems as $item) {
                if ($item->isHit() && $item->isTagged()) {
                    foreach ($item->getTagsVersions() as $tagKey => $tagVal) {
                        if (!$tagsItems[$tagKey]->isHit() || $tagsItems[$tagKey]->get() != $tagVal) {
                            $item->invalidate();
                            continue 2;
                        }
                    }
                }
            }
        }

        return $items;
    }

    /**
     * Confirms if the cache contains specified cache item.
     *
     * Note: This method MAY avoid retrieving the cached value for performance reasons.
     * This could result in a race condition with CacheItemInterface::get(). To avoid
     * such situation use CacheItemInterface::isHit() instead.
     *
     * @param string $key
     *   The key for which to check existence.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if item exists in the cache, false otherwise.
     */
    public function hasItem($key)
    {
        return $this->getItem($key)->isHit();
    }

    /**
     * Deletes all items in the pool.
     *
     * @return bool
     *   True if the pool was successfully cleared. False if there was an error.
     */
    public function clear()
    {
        return $this->itemProvider->clear();
    }

    /**
     * Removes the item from the pool.
     *
     * @param string $key
     *   The key to delete.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the item was successfully removed. False if there was an error.
     */
    public function deleteItem($key)
    {
        return $this->itemProvider->deleteItem($key);
    }

    /**
     * Removes multiple items from the pool.
     *
     * @param string[] $keys
     *   An array of keys that should be removed from the pool.
     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the items were successfully removed. False if there was an error.
     */
    public function deleteItems(array $keys)
    {
        return $this->itemProvider->deleteItems($keys);
    }

    /**
     * Persists a cache item immediately.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     *
     * @return bool
     *   True if the item was successfully persisted. False if there was an error.
     */
    public function save(CacheItemInterface $item)
    {
        if ($item instanceof DataAdapterTaggedInterface && $item->isTagged()) {
            $item->setTagsVersions(
                $this->saveTagsItems(
                    $this->getTagsItems($item->getTags())
                )
            );
        }

        return $this->itemProvider->save($item);
    }

    /**
     * Sets a cache item to be persisted later.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     * @return bool False if the item could not be queued or if a commit was attempted and failed. True otherwise.
     *   False if the item could not be queued or if a commit was attempted and failed. True otherwise.
     *
     * @throws CacheItemPoolExistItemException
     */
    public function saveDeferred(CacheItemInterface $item)
    {
        if (isset($this->deferredItems[$item->getKey()])) {
            throw new CacheItemPoolExistItemException();
        }

        $this->deferredItems[$item->getKey()] = $item;

        return true;
    }

    /**
     * Remove tags items from storage
     *
     * @param array $tags
     * @return bool
     */
    public function deleteTags(array $tags) : bool
    {
        $this->sessionTagsItems = array_intersect_key($this->sessionTagsItems, array_flip($tags));
        return $this->tagProvider->deleteItems($tags);
    }

    /**
     * @param array $tags
     * @return array
     */
    public function expireTags(array $tags) : array
    {
        return $this->saveTagsItems($this->tagProvider->createItems($tags));
    }

    /**
     * Persists any deferred cache items.
     *
     * @return bool
     *   True if all not-yet-saved items were successfully saved or there were none. False otherwise.
     */
    public function commit()
    {
        if ($this->deferredItems) {
            /** @var DataAdapterTaggedInterface[] $taggedItems */
            $taggedItems = [];
            $tagsMap = [];
            foreach ($this->deferredItems as $item) {
                if ($item instanceof DataAdapterTaggedInterface && $item->isTagged()) {
                    $tagsMap[] = $item->getTags();
                    $taggedItems[] = $item;
                } else {
                    $this->itemProvider->saveDeferred($item);
                }
            }

            if ($tagsMap) {
                $savedTags = $this->saveTagsItems(
                    $this->getTagsItems(array_merge(...$tagsMap))
                );

                foreach ($taggedItems as $item) {
                    $item->setTagsVersions(array_intersect_key($savedTags, $item->getTagsVersions()));
                    $this->itemProvider->saveDeferred($item);
                }
            }
        }
        
        return $this->itemProvider->commit();
    }

    /**
     * @param array $tags
     * @return DataAdapterInterface[]
     */
    private function getTagsItems(array $tags) : array
    {
        $tagsMap = array_flip($tags);
        $response = array_intersect_key($this->sessionTagsItems, $tagsMap);
        $notExistKey = array_diff_key($tagsMap, $response);
        if ($notExistKey) {
            foreach ($this->tagProvider->getItems(array_keys($notExistKey)) as $tagItem) {
                $response[$tagItem->getKey()] = $tagItem;
                if ($tagItem->isHit()) {
                    $this->sessionTagsItems[$tagItem->getKey()] = $tagItem;
                }
            }
        }

        return $response;
    }

    /**
     * @param CacheItemInterface[] $tagsItems
     * @return array
     * @throws CacheException
     */
    private function saveTagsItems(array $tagsItems) : array
    {
        $response = [];

        $unsaved = false;
        $tagValue = $this->getRandomTagVersion();
        foreach ($tagsItems as $tag) {
            if (!$tag->isHit()) {
                $tag->set($tagValue);
                $this->tagProvider->saveDeferred($tag);
                $unsaved = true;

                if (isset($this->sessionTagsItems[$tag->getKey()])) {
                    $this->sessionTagsItems[$tag->getKey()]->set($tag->get());
                }
            }

            $response[$tag->getKey()] = $tag->get();
        }

        if ($unsaved && !$this->tagProvider->commit()) {
            throw new CacheException('Cannot save tags');
        }

        return $response;
    }

    /**
     * @return string
     */
    private function getRandomTagVersion() : string
    {
        if (!self::$sessionTagPrefix) {
            self::$sessionTagPrefix = rand(10000, 99999);
        }

        return uniqid(self::$sessionTagPrefix);
    }
}