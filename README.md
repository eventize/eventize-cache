# Eventize Cache Component

This package contains PSR-6-compliant cache library.

## Install

```
composer require andrej-in-ua/eventize-cache
```

## Usage manually

```php
<?php

class MyClass
{
    public function __construct(
        \Eventize\Cache\CacheProviderInterface $cacheItemProvider,
        \Eventize\Cache\CacheProviderInterface $cacheTagsProvider
    )
    {
        $this->cacheTagManager = new \Eventize\Cache\CacheTagManager($cacheItemProvider, $cacheTagsProvider);
    }

    public function someMethod()
    {
        $item = $this->cacheTagManager->getItem('any-key');
        if (!$item->isHit()) {
            $item->setTags(['item-tag-1', 'item-tag-2'])
                ->set('any json encodeble data');

            $this->cacheTagManager->save($item);
        }

        return $item->get();
    }
}
```
